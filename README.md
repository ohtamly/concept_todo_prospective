# TODOの先行き予測データ構造
ゴールに対してどのくらい進捗があったのか、どれくらい先が見えているのかを推測する概念的データ構造を思いつくままGoで記録します。

特に使われるシチュエーションを考慮していません。

## 動作環境
- macOS 11.1+
- Golang 1.15

## 使い方

上記どおり、使われるシチュエーションを考慮していませんが、概ね下記のように使う感じです。

```go
package main

import (
    "fmt"
    "concepts"
)

var todoNode concepts.Node

func init() {
	todoNode = concepts.NewNode()
	todo1 := concepts.NewNode()
	{
		child1 := concepts.NewTerminalNode()
		child2 := concepts.NewTerminalNode()
		child3 := concepts.NewTerminalNode()
		child1.SetDone()
		child2.SetDone()
		child3.SetDone()
		todo1.Add(child1, child2, child3)
	}
	todo2 := concepts.NewNode()
	{
		child1 := concepts.NewTerminalNode()
		child2 := concepts.NewTerminalNode()
		child1.SetDone()
		todo2.Add(child1, child2)
	}
	todo3 := concepts.NewNode()
	{
		child1 := concepts.NewTerminalNode()
		child2 := concepts.NewTerminalNode()
		child3 := concepts.NewTerminalNode()
		child4 := concepts.NewTerminalNode()
		todo3.Add(child1, child2, child3, child4)
	}
	todoNode.Add(todo1, todo2, todo3)
}

func main() {
    tp := concepts.TodoProspective(todoNode)
    fmt.Println("TodoProspective: %g", tp)
}
```

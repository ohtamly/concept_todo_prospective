package concepts

type TodoNode interface {
	Done() bool
	ExpectedSuccessfulDepth() (float32, bool)
	Depth() (min, max int, average float32)
}

// TodoProspective TODOリストの先行きがはっきり見えている度合いを返す
func TodoProspective(t TodoNode) float32 {
	expectedDepth, reliable := t.ExpectedSuccessfulDepth()
	if reliable {
		_, _, average := t.Depth()
		t := average / expectedDepth
		if t > 1.0 {
			return 1.0
		} else {
			return t
		}
	} else {
		return 0.0
	}
}

// -----

type Node struct {
	children []TodoNode
}

// NewNode Nodeコンストラクター
func NewNode() *Node {
	return &Node{}
}

// Add 子ノードを追加する
func (n *Node) Add(t ...TodoNode) {
	n.children = append(n.children, t...)
}

// Done 完了しているかどうかを返す
func (n *Node) Done() bool {
	for _, child := range n.children {
		if !child.Done() {
			return false
		}
	}
	return true
}

// ExpectedSuccessfulDepth 達成できているTODOノードから達成にかかる子ノードの深さの期待値を計算する
// 達成したノードがない場合は期待値を計算できないため、 reliable を false で返す。
func (n *Node) ExpectedSuccessfulDepth() (depth float32, reliable bool) {
	sum := float32(0)
	samples := 0
	for _, child := range n.children {
		d, rel := child.ExpectedSuccessfulDepth()
		if rel {
			samples++
			sum += d + 1.0
		}
	}
	if samples == 0 {
		return 0.0, false
	} else {
		return sum / float32(samples), true
	}
}

// Depth ノードの深さを取得する
func (n *Node) Depth() (min, max int, average float32) {
	sum := float32(0)
	for _, child := range n.children {
		mn, mx, avg := child.Depth()
		if min == 0 || min > mn+1.0 {
			min = mn + 1.0
		}
		if max == 0 || max < mx+1.0 {
			max = mx + 1.0
		}
		sum += avg + 1.0
	}
	average = sum / float32(len(n.children))
	return
}

// -----

type TerminalNode struct {
	done bool
}

// NewTerminalNode TerminalNodeコンストラクター
func NewTerminalNode() *TerminalNode {
	return &TerminalNode{}
}

// Done 完了しているかどうかを返す
func (t *TerminalNode) Done() bool {
	return t.done
}

// SetDone 完了とする
func (t *TerminalNode) SetDone() {
	t.done = true
}

// ExpectedSuccessfulDepth 達成できているTODOノードから達成にかかる子ノードの深さの期待値を計算する
// 達成したノードがない場合は期待値を計算できないため、 reliable を false で返す。
func (t *TerminalNode) ExpectedSuccessfulDepth() (depth float32, reliable bool) {
	if t.done {
		return 1.0, true
	} else {
		return 0.0, false
	}
}

// Depth ノードの深さを取得する (末端なので深さはない)
func (t *TerminalNode) Depth() (min, max int, average float32) {
	return 0.0, 0.0, 0.0
}
